> repo moved to [git.ant.lgbt/experiments/tic-tac-toe](https://git.ant.lgbt/experiments/tic-tac-toe)

# Game: Text-Based Tic Tac Toe, written in C

This is a fully functional TicTacToe game that I wrote in C to work in the console, it has no dependencies and is made very simple. Don’t expect an amazing gaming experience as this is mainly just a project for myself and not very focused on the UI at all, it’s functional but not very UI-focused.
